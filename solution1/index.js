const BASE_URL = 'http://localhost:8080/';

document.addEventListener('DOMContentLoaded', async () => {
  const fetchCouncillors = async () => {
    const response = await fetch(`${BASE_URL}councillors`);
    return await response.json();
  };

  const data = await fetchCouncillors();

  const tbody = document.querySelector('tbody');

  data.forEach((item) => {
    const row = document.createElement('tr');

    const id = document.createElement('td');
    id.innerText = item.id;

    const number = document.createElement('td');
    number.innerText = item.number || 0;

    const firstName = document.createElement('td');
    firstName.innerText = item.firstName;

    const lastName = document.createElement('td');
    lastName.innerText = item.lastName;

    row.appendChild(id);
    row.appendChild(number);
    row.appendChild(firstName);
    row.appendChild(lastName);

    tbody.appendChild(row);
  });
});
