import React from 'react';

const Input = ({ label, ...rest }) => (
  <label className="flex items-center gap-1">
    <span>{label}:</span>
    <input
      {...rest}
      className="border border-gray-300 rounded p-1 hover:border-gray-400"
    />
  </label>
);

export default Input;
