import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import clsx from 'clsx';

const NAV_ITEMS = [
  { label: 'Councillors', href: 'councillors' },
  { label: 'Affairs', href: 'affairs' },
  { label: 'Councils', href: 'councils' },
];

const Navbar = () => {
  const { pathname } = useLocation();

  return (
    <nav className="border-b border-gray-300 bg-gray-100 shadow-lg">
      <ul className="flex items-center p-4 gap-8">
        {NAV_ITEMS.map((item) => (
          <li
            className={clsx(
              'hover:underline text-lg',
              pathname.includes(item.href) && 'underline'
            )}
            key={item.href}
          >
            <Link to={item.href}>{item.label}</Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navbar;
