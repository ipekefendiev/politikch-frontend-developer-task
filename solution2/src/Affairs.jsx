import React, { useEffect, useMemo, useState } from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import dayjs from 'dayjs';

import Input from 'components/Input';
import Table from 'components/Table';

const Affairs = () => {
  const columnHelper = createColumnHelper();

  const [data, setData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [dateValue, setDateValue] = useState('');

  useEffect(() => {
    const fetchAffairs = async () => {
      const response = await fetch('http://localhost:8080/affairs');
      const responseData = await response.json();

      setData(responseData);
    };

    fetchAffairs();
  }, []);

  const columns = [
    columnHelper.accessor('id', {
      header: () => 'ID',
    }),
    columnHelper.accessor('shortId', {
      header: () => 'Short ID',
    }),
    columnHelper.accessor('updated', {
      header: () => 'Date',
      cell: (info) => dayjs(info.getValue()).format('MM/DD/YYYY'),
    }),
  ];

  const filteredData = useMemo(() => {
    let clonedData = [...data];

    if (dateValue) {
      clonedData = clonedData.filter((item) => {
        const formattedDate = dayjs(item.updated).format('MM/DD/YYYY');
        return formattedDate.includes(dateValue);
      });
    }

    return clonedData;
  }, [data, dateValue]);

  return (
    <div className="container mx-auto my-6 px-4 flex flex-col">
      <h1
        className="text-black font-bold text-4xl mx-auto mb-8 text-transparent bg-clip-text bg-gradient-to-r
      from-teal-500 to-blue-600"
      >
        Affairs
      </h1>
      <div className="flex items-center gap-8 mb-4">
        <Input
          onChange={(e) => setDateValue(e.target.value)}
          value={dateValue}
          label="Date Filter"
          placeholder="MM/DD/YYYY"
        />
      </div>
      <Table
        columns={columns}
        data={filteredData}
        sorting={sorting}
        setSorting={setSorting}
      />
    </div>
  );
};

export default Affairs;
