import { useState, useMemo, useEffect } from 'react';
import { createColumnHelper } from '@tanstack/react-table';

import Table from 'components/Table';
import Input from 'components/Input';

import './App.css';

function App() {
  const columnHelper = createColumnHelper();

  const [data, setData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [idFilterValue, setIdFilterValue] = useState('');
  const [firstNameFilterValue, setFirstNameFilterValue] = useState('');

  useEffect(() => {
    const fetchCouncillors = async () => {
      const response = await fetch('http://localhost:8080/councillors');
      const responseData = await response.json();

      setData(responseData);
    };

    fetchCouncillors();
  }, []);

  const filteredData = useMemo(() => {
    let clonedData = [...data];

    if (idFilterValue) {
      clonedData = clonedData.filter((item) =>
        item.id.toString().includes(idFilterValue)
      );
    }
    if (firstNameFilterValue) {
      clonedData = clonedData.filter((item) =>
        item.firstName
          .toLowerCase()
          .includes(firstNameFilterValue.toLowerCase())
      );
    }

    return clonedData;
  }, [data, idFilterValue, firstNameFilterValue]);

  const columns = [
    columnHelper.accessor('id', {
      header: () => 'ID',
    }),
    columnHelper.accessor('number', {
      header: () => 'Number',
      cell: (info) => info.getValue() || 0,
    }),
    columnHelper.accessor('firstName', {
      header: () => 'First Name',
    }),
    columnHelper.accessor('lastName', {
      header: () => 'Last Name',
    }),
  ];

  return (
    <div className="container mx-auto my-6 px-4 flex flex-col">
      <h1
        className="text-black font-bold text-4xl mx-auto mb-8 text-transparent bg-clip-text bg-gradient-to-r
      from-teal-500 to-blue-600"
      >
        Councillors
      </h1>
      <div className="flex items-center gap-8 mb-4">
        <Input
          onChange={(e) => setIdFilterValue(e.target.value)}
          value={idFilterValue}
          label="ID Filter"
        />
        <Input
          onChange={(e) => setFirstNameFilterValue(e.target.value)}
          value={firstNameFilterValue}
          label="First Name Filter"
        />
      </div>
      <Table
        columns={columns}
        data={filteredData}
        sorting={sorting}
        setSorting={setSorting}
      />
    </div>
  );
}

export default App;
