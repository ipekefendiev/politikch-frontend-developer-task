import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

import Navbar from 'components/Navbar';
import Councillors from 'Councillors';
import Councils from 'Councils';
import Affairs from 'Affairs';

import './index.css';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/councillors" element={<Councillors />} />
        <Route path="/affairs" element={<Affairs />} />
        <Route path="/councils" element={<Councils />} />
        <Route path="*" element={<Navigate to="/councillors" replace />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
