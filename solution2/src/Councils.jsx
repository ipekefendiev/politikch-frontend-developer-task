import React, { useEffect, useMemo, useState } from 'react';
import { createColumnHelper } from '@tanstack/react-table';

import Input from 'components/Input';
import Table from 'components/Table';

const Councils = () => {
  const columnHelper = createColumnHelper();

  const [data, setData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [nameValue, setNameValue] = useState('');

  useEffect(() => {
    const fetchCouncils = async () => {
      const response = await fetch('http://localhost:8080/councils');
      const responseData = await response.json();

      setData(responseData);
    };

    fetchCouncils();
  }, []);

  const columns = [
    columnHelper.accessor('id', {
      header: () => 'ID',
    }),
    columnHelper.accessor('abbreviation', {
      header: () => 'Abbreviation',
    }),
    columnHelper.accessor('code', {
      header: () => 'Code',
    }),
    columnHelper.accessor('name', {
      header: () => 'Name',
    }),
    columnHelper.accessor('type', {
      header: () => 'Type',
    }),
  ];

  const filteredData = useMemo(() => {
    let clonedData = [...data];

    if (nameValue) {
      clonedData = clonedData.filter((item) =>
        item.name.toLowerCase().includes(nameValue.toLowerCase())
      );
    }

    return clonedData;
  }, [data, nameValue]);

  return (
    <div className="container mx-auto my-6 px-4 flex flex-col">
      <h1
        className="text-black font-bold text-4xl mx-auto mb-8 text-transparent bg-clip-text bg-gradient-to-r
      from-teal-500 to-blue-600"
      >
        Councils
      </h1>
      <div className="flex items-center gap-8 mb-4">
        <Input
          onChange={(e) => setNameValue(e.target.value)}
          value={nameValue}
          label="Name Filter"
        />
      </div>
      <Table
        columns={columns}
        data={filteredData}
        sorting={sorting}
        setSorting={setSorting}
      />
    </div>
  );
};

export default Councils;
