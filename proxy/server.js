import express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';

const app = express();

app
  .use(
    express.urlencoded({
      extended: true,
    })
  )
  .use(express.json())
  .use(cors());

const routes = express.Router();

const BASE_URL = 'http://ws-old.parlament.ch/';

routes.get('/councillors', async (req, res) => {
  const response = await fetch(`${BASE_URL}councillors?format=json`, {
    method: 'GET',
    headers: {
      Accept: 'text/json',
      'Content-Type': 'application/json',
    },
  });
  const data = await response.json();
  res.json(data);
});

routes.get('/affairs', async (req, res) => {
  const response = await fetch(`${BASE_URL}affairs?format=json`, {
    method: 'GET',
    headers: {
      Accept: 'text/json',
      'Content-Type': 'application/json',
    },
  });
  const data = await response.json();
  res.json(data);
});

routes.get('/councils', async (req, res) => {
  const response = await fetch(`${BASE_URL}councils?format=json`, {
    method: 'GET',
    headers: {
      Accept: 'text/json',
      'Content-Type': 'application/json',
    },
  });
  const data = await response.json();
  res.json(data);
});

app.use('/', routes);

app.listen(8080, () => {
  console.log('listening 8080');
});
